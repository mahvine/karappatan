package ph.mahvine.karappatan.service.dto;

import ph.mahvine.karappatan.config.Constants;

import ph.mahvine.karappatan.domain.Authority;
import ph.mahvine.karappatan.domain.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.*;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
@JsonInclude(Include.NON_NULL)
public class UpdateProfileDTO {

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    private String email;

    @Size(max = 256)
    private String imageUrl;

    @Size( max = 30)
    private String contactNumber;

    @Size(max = 254)
    private String address;
    
    public UpdateProfileDTO() {
        // Empty constructor needed for Jackson.
    }

    public UpdateProfileDTO(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();

        this.imageUrl = user.getImageUrl();
        this.contactNumber = user.getContactNumber();
        this.address = user.getAddress();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "UpdateProfileDTO ["
				+ (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (lastName != null ? "lastName=" + lastName + ", " : "")
				+ (email != null ? "email=" + email + ", " : "")
				+ (imageUrl != null ? "imageUrl=" + imageUrl + ", " : "")
				+ (contactNumber != null ? "contactNumber=" + contactNumber + ", " : "")
				+ (address != null ? "address=" + address : "") + "]";
	}
    

    

    
}
