package ph.mahvine.karappatan.service.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ph.mahvine.karappatan.domain.enumeration.CaseSummaryType;

/**
 * A DTO for the CaseSummary entity.
 */
public class CreateCaseSummaryDTO implements Serializable {

    private List<Long> answerIds = new ArrayList<>();
    
	private String details;
	
	private String contactNumber;

	private String location;

	private CaseSummaryType type;

	public List<Long> getAnswerIds() {
		return answerIds;
	}

	public void setAnswerIds(List<Long> answerIds) {
		this.answerIds = answerIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answerIds == null) ? 0 : answerIds.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreateCaseSummaryDTO other = (CreateCaseSummaryDTO) obj;
		if (answerIds == null) {
			if (other.answerIds != null)
				return false;
		} else if (!answerIds.equals(other.answerIds))
			return false;
		return true;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "{" 
		+ (answerIds != null ? "answerIds:" + answerIds + ", " : "")
		+ (details != null ? "details:" + details : "")
		+ (location != null ? "location:" + location : "")
		+ "}";
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public CaseSummaryType getType() {
		return type;
	}

	public void setType(CaseSummaryType type) {
		this.type = type;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
    
    

}
