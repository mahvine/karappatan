package ph.mahvine.karappatan.domain.enumeration;

/**
 * The CaseSummaryType enumeration.
 */
public enum CaseSummaryType {
    INDIGENT, PREMIUM
}
